<?php

namespace App\Http\Livewire;

use Cart;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CartComponent extends Component
{
    public function increaseQuantity($rowId)//ürün sayaç artış miktarı
    {
        $product = Cart::get($rowId);
        $qty = $product->qty + 1;
        Cart::update($rowId, $qty);
    }

    public function decreaseQuantity($rowId)//ürün sayaç azalış miktarı
    {
        $product = Cart::get($rowId);
        $qty = $product->qty - 1;
        Cart::update($rowId, $qty);
    }

    public function destroy($rowId) // sepetten kalem ürün silme fonksiyonu
    {
        Cart::remove($rowId);
        session()->flash('success_message', 'Ürün Sepetinizden Kaldırılmıştır.');
    }

    public function destroyAll() // tüm sepette ki ürünleri silme (sepet boşaltma) fonksiyonu
    {
        Cart::destroy();
    }

    public function checkout()
    {
        if (Auth::check()) {
            return redirect()->route('checkout');
        } else {
            return redirect()->route('login');
        }
    }

    public function setAmountForChackout()
    {
        if (!Cart::instance('cart')->count() > 0){
            session()->forget('checkout');
            return;
        }
        if (session()->has('coupon')) {
            session()->put('checkout', [
                'discount' => $this->discount,
                'subtotal' => $this->subtotalAfterDiscount,
                'tax' => $this->taxAfterDiscount,
                'total' => $this->totalAfterDiscount,
            ]);
        } else {
            session()->put('checkout', [
                'discount' => 0,
                'subtotal' => Cart::instance('cart')->subtotal(),
                'tax' => Cart::instance('cart')->tax(),
                'total' => Cart::instance('cart')->total(),
            ]);
        }
    }

    public function render()
    {
        return view('livewire.cart-component')->layout("layouts.base");
    }
}
