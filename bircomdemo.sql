-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 31 May 2021, 09:09:15
-- Sunucu sürümü: 10.4.18-MariaDB
-- PHP Sürümü: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `bircomdemo`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'quaerat iusto', 'quaerat-iusto', '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(2, 'praesentium voluptas', 'praesentium-voluptas', '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(3, 'non accusamus', 'non-accusamus', '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(4, 'et assumenda', 'et-assumenda', '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(5, 'error asperiores', 'error-asperiores', '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(6, 'vel veritatis', 'vel-veritatis', '2021-05-31 03:49:39', '2021-05-31 03:49:39');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2021_05_28_075830_create_sessions_table', 1),
(7, '2021_05_28_104559_create_categories_table', 1),
(8, '2021_05_28_110059_create_products_table', 1),
(9, '2021_05_29_194849_create_orders_table', 1),
(10, '2021_05_29_194911_create_order_items_table', 1),
(11, '2021_05_29_194959_create_shippings_table', 1),
(12, '2021_05_29_195042_create_transactions_table', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `discount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `tax` decimal(8,2) NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `line1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `line2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ordered','delivered','canceled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ordered',
  `is_shipping_different` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `regular_price` decimal(8,2) NOT NULL,
  `sale_price` decimal(8,2) DEFAULT NULL,
  `SKU` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_status` enum('instock','outstock') COLLATE utf8mb4_unicode_ci DEFAULT 'instock',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 10,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `short_description`, `description`, `regular_price`, `sale_price`, `SKU`, `stock_status`, `featured`, `quantity`, `image`, `images`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'aut et cupiditate quis', 'aut-et-cupiditate-quis', 'Modi aut aut et perferendis ipsum. Laborum et voluptatibus rem maiores. Voluptas ea sit quia ipsum laboriosam et voluptatum. Ex ex et soluta ad. Error ea blanditiis enim et id ut eaque.', 'Aliquid quam quam quibusdam fuga necessitatibus voluptatum fugit. Quia ut saepe sed quaerat facilis a dolorem. Ut voluptatibus recusandae ex nihil vero eligendi ducimus ullam. Omnis qui minus non voluptatibus eveniet. Ut neque voluptatum error est minus ex. Quo eaque neque mollitia odio. Qui cum omnis quasi repudiandae. Voluptas quia sunt sed. Dolores quia alias accusantium.', '278.00', NULL, 'DIGI495', 'instock', 0, 164, 'digital_11.jpg', NULL, 3, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(2, 'quia et voluptatem sit', 'quia-et-voluptatem-sit', 'Possimus et qui qui doloremque dolor. Culpa deleniti magnam voluptatem error placeat adipisci qui asperiores. Quisquam modi et eveniet at cumque similique molestiae. Veritatis voluptas ipsa tempore.', 'Explicabo sed tempore soluta. Debitis ullam a non amet. Molestiae enim aperiam dolor omnis laboriosam et accusantium. Et odit sed est repellendus rerum aperiam. Autem dignissimos at aut dolor aliquid. Velit vitae praesentium eligendi magni et magni adipisci. Occaecati velit non in perferendis natus ipsum dolore. Accusantium qui ex culpa iusto non ducimus. Et eveniet ut veritatis in et itaque.', '312.00', NULL, 'DIGI279', 'instock', 0, 153, 'digital_22.jpg', NULL, 4, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(3, 'dolor ipsa placeat qui', 'dolor-ipsa-placeat-qui', 'Alias et autem rerum nihil. Modi vel vitae neque. Omnis natus dicta neque fugit cupiditate enim. Explicabo natus et nesciunt incidunt est consequatur. Et ab non dolores minima.', 'Nihil repellat quas placeat porro. Voluptatem excepturi tempora enim reprehenderit. Placeat est non provident ut. Aut quae est delectus nemo. Vel unde voluptas voluptatem dolores est asperiores. Provident recusandae consequuntur consequuntur aut eum voluptatibus laborum. Iste id ea est esse veritatis veritatis dicta.', '492.00', NULL, 'DIGI202', 'instock', 0, 116, 'digital_6.jpg', NULL, 5, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(4, 'ratione eos rerum aut', 'ratione-eos-rerum-aut', 'Molestiae impedit et cum vitae veritatis nihil. Placeat fugit tempora consequatur quidem. Consequatur recusandae ex consequatur ab deleniti ullam. Qui nihil minima omnis nihil culpa velit.', 'Id possimus occaecati ducimus facere aperiam qui rerum est. Et dolores velit illum molestiae libero dolore qui enim. Est impedit officia exercitationem culpa mollitia. Inventore veritatis consequatur voluptates fugiat voluptatibus et. Dolores odit corrupti libero autem ea vel voluptates. Adipisci quo consequatur aspernatur porro perferendis quis. Iste animi sint ipsa quia molestiae aliquam est atque.', '261.00', NULL, 'DIGI484', 'instock', 0, 102, 'digital_16.jpg', NULL, 5, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(5, 'omnis numquam voluptatem et', 'omnis-numquam-voluptatem-et', 'Dicta laboriosam atque autem nihil harum. Corporis quae recusandae voluptate ducimus nihil minima. Animi quia ut accusamus sapiente unde totam.', 'Aspernatur fugiat ullam ullam nobis nihil. In sed consectetur nulla maiores at voluptatem quae. Accusamus labore sit cupiditate delectus odit magni et corrupti. Reiciendis architecto blanditiis qui quod asperiores. Distinctio quibusdam veniam est et qui eum qui consectetur. Eveniet commodi alias enim illo id. Facilis fugiat numquam repellendus asperiores. Suscipit assumenda et fugit. Corporis ut perferendis nulla et tempore voluptatem est.', '166.00', NULL, 'DIGI259', 'instock', 0, 189, 'digital_19.jpg', NULL, 5, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(6, 'fuga voluptates et consectetur', 'fuga-voluptates-et-consectetur', 'Explicabo alias rerum aut tempore. Est sequi occaecati aliquam alias dolor. Est deleniti eaque quos nam cumque in nemo.', 'Impedit et corporis sit voluptas. Quidem cum et laboriosam rerum in dolores laudantium totam. Natus voluptatem suscipit aut id ipsam culpa. Et earum tempora rerum nostrum hic aut. Nobis corporis voluptas eos nesciunt natus animi quam. Dolorem aut earum qui totam suscipit soluta. Aut qui consequatur ipsa corporis. Expedita occaecati est quas maiores ut blanditiis. Aut quos unde rerum est provident.', '112.00', NULL, 'DIGI282', 'instock', 0, 102, 'digital_3.jpg', NULL, 2, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(7, 'eum voluptas aut quas', 'eum-voluptas-aut-quas', 'Rerum doloribus voluptatem odit ad molestiae cum. Animi debitis et impedit aspernatur non ea. Dolor eligendi omnis sit deleniti officiis id tempore. Officiis rerum et aut neque laboriosam.', 'Mollitia minima quis a ea officia vel nam. Sequi consequatur itaque repellat sint iure nam quos dolorum. Explicabo non voluptatem amet sapiente consequatur ut. Error sed itaque vel tempora incidunt ad. Tenetur architecto harum fuga molestias fugiat tempora. Odit exercitationem accusantium at in nostrum. Labore repudiandae nobis corporis officia officiis exercitationem deserunt aut. Eos rerum repudiandae velit illum. Illo et quas fugiat sapiente aut est. Saepe aliquam ea ab dolor ut dicta.', '87.00', NULL, 'DIGI212', 'instock', 0, 180, 'digital_1.jpg', NULL, 4, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(8, 'ut velit inventore dolores', 'ut-velit-inventore-dolores', 'Eum aperiam eum dicta expedita ratione. Voluptas ut ut quis eos quisquam. Possimus tempore exercitationem enim aut qui.', 'Suscipit iure eum cupiditate debitis saepe excepturi excepturi. Harum ex non voluptas. Eligendi rerum deleniti praesentium occaecati. Sunt at maiores illum. Enim nostrum autem similique et. Deserunt voluptatum impedit sint placeat temporibus. Commodi sed nisi odit mollitia ratione vel. Dolorem eum tempora et eveniet beatae aliquid facere. Eius maiores in reprehenderit in. Natus sed unde reiciendis nobis.', '35.00', NULL, 'DIGI283', 'instock', 0, 165, 'digital_9.jpg', NULL, 2, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(9, 'asperiores repellat impedit aut', 'asperiores-repellat-impedit-aut', 'Culpa ratione saepe ad ut adipisci. Nisi distinctio rem pariatur saepe aperiam. Qui fuga non nihil optio voluptatem.', 'Voluptatem eius id consectetur. In omnis fugit facere distinctio accusamus fugiat quia. Et suscipit voluptatem est iure. Ullam id voluptates quod ut culpa quia iure. Natus fugiat fugit quidem magnam maiores. Explicabo nemo ex in aut similique ex. Voluptatum assumenda et aut et ex. Consequatur incidunt voluptatibus voluptatem dignissimos exercitationem fugit blanditiis.', '210.00', NULL, 'DIGI324', 'instock', 0, 136, 'digital_13.jpg', NULL, 3, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(10, 'ut esse nisi ad', 'ut-esse-nisi-ad', 'Excepturi modi labore vitae praesentium quia voluptas. Ea optio voluptate illo cumque. Labore illum voluptatem magni dolorem magni et molestiae. Similique fuga ut quaerat nam consequatur.', 'Non et perferendis pariatur et ipsam. Ex exercitationem voluptate dolor officiis sapiente minima. Suscipit iure fuga laborum. Ut voluptatem culpa a ut dolor. Ipsam consequuntur harum qui suscipit provident. Consequatur in beatae inventore odio nulla. Minus eum totam alias dolorum. Aut voluptas tenetur deleniti nemo vero non earum. Delectus nemo iste numquam alias ad eum. Eos qui aspernatur placeat et a. Eligendi et id earum.', '286.00', NULL, 'DIGI157', 'instock', 0, 116, 'digital_8.jpg', NULL, 2, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(11, 'vel quas quas nesciunt', 'vel-quas-quas-nesciunt', 'Accusamus et voluptatem quibusdam facilis commodi. Dolore id consectetur expedita voluptatibus. Nisi blanditiis in doloribus cupiditate sunt. Magnam deleniti earum eligendi et iusto omnis.', 'Qui commodi earum quidem. Enim rerum commodi recusandae consequatur debitis possimus. Eligendi facere sed velit veritatis sequi distinctio eum. Alias nobis expedita sit ea similique sint. Ab natus commodi id magni reiciendis expedita sapiente quaerat. Deleniti qui non earum temporibus enim eveniet.', '434.00', NULL, 'DIGI442', 'instock', 0, 195, 'digital_2.jpg', NULL, 5, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(12, 'fugiat numquam cumque voluptas', 'fugiat-numquam-cumque-voluptas', 'Animi eum qui soluta ut commodi deleniti. Magnam et beatae qui doloribus voluptatibus magnam minus. A beatae et sunt placeat occaecati non.', 'Minima ut quaerat eos tenetur. Error occaecati nisi pariatur porro qui beatae. Aut iusto unde quod voluptatum quis repudiandae harum dolores. Ut quasi voluptas eius voluptas quia accusamus neque. Consequuntur quia possimus rerum placeat fugiat quia architecto. Et cumque est dolores officiis nihil incidunt reprehenderit suscipit. Totam officiis expedita dolores iure suscipit architecto est qui. Libero non vel ratione aliquam qui incidunt at.', '234.00', NULL, 'DIGI145', 'instock', 0, 190, 'digital_4.jpg', NULL, 3, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(13, 'ipsum pariatur officiis dolore', 'ipsum-pariatur-officiis-dolore', 'Illo sit est alias est a. Neque fuga nobis ut delectus. Omnis et rerum ad et aliquid ipsam nam. Quaerat non ducimus ex veritatis quidem aspernatur laudantium est.', 'Fuga et quam harum sunt atque hic earum. Qui fugiat harum dolore reiciendis. Qui eos dolor corrupti earum mollitia. Facilis ut placeat dolorem autem. Omnis explicabo eos cumque magnam sed. Illum laboriosam dolorum tempore ipsam consequatur harum. Maiores dolorem sed fugit qui. Exercitationem nisi corrupti consectetur enim quos modi ipsam. Quod inventore ipsam ut laborum. Tempore dolor enim est consequatur nihil corrupti consectetur facilis.', '327.00', NULL, 'DIGI213', 'instock', 0, 172, 'digital_5.jpg', NULL, 5, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(14, 'qui quia occaecati expedita', 'qui-quia-occaecati-expedita', 'Dolores in aperiam ducimus veritatis ratione facere. Voluptatem et quis ut et aliquid repellendus. Non officiis cupiditate voluptates molestiae. Est ut distinctio hic voluptas minima sint.', 'Neque sit et fuga sit odit. Omnis quibusdam omnis et officia est voluptatibus quia. Qui aut corporis et quae ut eaque. Voluptate eum exercitationem aut omnis natus nostrum. Facere veniam dolores suscipit voluptates quidem est architecto. Animi cumque autem laudantium quia explicabo inventore quia. Aliquam sit dolor rem ea et.', '168.00', NULL, 'DIGI216', 'instock', 0, 134, 'digital_12.jpg', NULL, 3, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(15, 'voluptas sequi est eum', 'voluptas-sequi-est-eum', 'Omnis numquam voluptatem et iusto. Iure possimus ullam voluptatum aliquid eligendi fugiat. Sit est soluta est dolores fuga dignissimos fugit.', 'Et libero assumenda ipsum voluptas veniam ut officia commodi. Quas minima magni sequi et. Quod id non et. Optio ducimus vel dolorem rerum. Laudantium in iure at sunt omnis. Modi nihil dolores delectus error non. Ut qui ipsam rerum qui tempore. Et qui corporis qui provident amet vero eligendi. Debitis qui possimus nam itaque modi animi voluptatem. Voluptatibus ut debitis molestiae tenetur harum enim aut. At tenetur recusandae et. Nihil blanditiis omnis in asperiores.', '433.00', NULL, 'DIGI414', 'instock', 0, 187, 'digital_7.jpg', NULL, 4, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(16, 'odio velit deserunt ipsum', 'odio-velit-deserunt-ipsum', 'Ut mollitia minima molestias cumque quia. Est alias aliquam voluptatem eveniet est ex. Soluta libero nesciunt praesentium saepe quis.', 'Iure voluptatem maiores atque veritatis. Quidem qui a quos tenetur. Dolor sequi voluptatem aut ex molestias. Voluptatum reiciendis vitae inventore modi repellendus qui dignissimos. Laboriosam omnis perferendis molestiae ut repellat. Expedita et quia nulla. Velit sit qui fugit sequi aut molestiae. Rerum harum saepe quos ea necessitatibus pariatur. Illum et ut expedita ut omnis non vitae. Mollitia nihil ex qui alias. Aliquam dolor impedit et ullam corrupti.', '177.00', NULL, 'DIGI170', 'instock', 0, 170, 'digital_14.jpg', NULL, 4, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(17, 'voluptatem sapiente vel voluptatem', 'voluptatem-sapiente-vel-voluptatem', 'Quidem et excepturi eius quia. Sapiente deleniti aut dolorem error omnis enim. Eveniet repudiandae ratione eos libero totam. Sapiente qui ipsam ut. Voluptatem quo rem itaque ratione aut voluptatem.', 'Dolores dolorum sed blanditiis laborum dolorum rem aspernatur. Dolor recusandae corrupti labore consequuntur consectetur numquam. Non velit labore perspiciatis aperiam nisi sit et. Est et dolor aut praesentium odit. Non a illum natus et tempore perferendis dicta. Nihil quia dolores et aperiam ea. Sunt explicabo temporibus molestiae numquam aut.', '282.00', NULL, 'DIGI291', 'instock', 0, 128, 'digital_18.jpg', NULL, 3, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(18, 'in corporis atque perspiciatis', 'in-corporis-atque-perspiciatis', 'At error modi non. Eveniet est error fuga modi consequuntur. Sed ipsa explicabo voluptatibus.', 'Saepe sed sint quia quos eum molestias ex. Dolorem quo esse dolores velit facere. Sed placeat in amet dicta similique et deleniti. Nihil sed unde id totam. Aut et eaque non voluptas aut. Labore vel iure et nihil neque enim est. Officia id ut rem illum architecto. A voluptatum aut tenetur sint quam sunt molestiae modi.', '26.00', NULL, 'DIGI219', 'instock', 0, 195, 'digital_15.jpg', NULL, 1, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(19, 'et magnam consequatur sit', 'et-magnam-consequatur-sit', 'Aliquam animi non dolor accusantium esse est tempore ipsa. Quas non quo aut necessitatibus rerum.', 'Non et consequatur nostrum rem itaque et. Sunt est qui occaecati nam quas dolores culpa. Eius rerum sapiente labore et. Inventore ut ut id ab. Odio esse laboriosam et aut corrupti enim ex. Optio et neque veritatis fugiat voluptatem quaerat. Quibusdam est eius eveniet necessitatibus. Placeat aut iste reprehenderit accusantium quidem nesciunt aspernatur. Cupiditate fugit ut est ut rerum sapiente. Inventore sunt quaerat sunt blanditiis animi.', '363.00', NULL, 'DIGI224', 'instock', 0, 120, 'digital_10.jpg', NULL, 2, '2021-05-31 03:49:39', '2021-05-31 03:49:39'),
(20, 'laborum porro amet magni', 'laborum-porro-amet-magni', 'Beatae unde distinctio accusantium est eos. Est debitis ad fugiat ea repellat minus. Incidunt et quia maxime molestiae expedita dolorem perspiciatis. Consequatur reprehenderit in dolor.', 'Suscipit alias voluptas quibusdam vero voluptas. Asperiores quam ipsam et ratione ratione placeat libero ratione. Qui sunt perferendis consectetur quia. Odio dolor culpa minus sed eveniet veniam. Similique ut pariatur iste dolorum eius omnis aut beatae. Sed esse ut voluptas dolores laborum autem. In corrupti doloremque cum suscipit modi eum. Ex enim earum autem laboriosam id qui beatae et. Fugit quas placeat eius est. Magni debitis quia et consequatur eveniet sit iure dolore.', '301.00', NULL, 'DIGI476', 'instock', 0, 197, 'digital_21.jpg', NULL, 3, '2021-05-31 03:49:40', '2021-05-31 03:49:40'),
(21, 'commodi autem quaerat eum', 'commodi-autem-quaerat-eum', 'Iste aut assumenda earum est. Earum non ipsam et accusantium sed vero molestiae. Inventore rerum suscipit atque iste officia consequuntur.', 'Rerum et nulla commodi. Ipsum aut enim laborum. Neque quo accusamus unde rerum nisi suscipit odit repudiandae. Ab et minima at. Voluptates molestiae enim et harum assumenda ea architecto. Molestiae consequatur quas doloremque voluptates dolorem quas neque expedita. Laboriosam labore nesciunt et a ipsa ut. Molestiae et id qui ratione. Atque repudiandae suscipit eos nesciunt eum consequatur. Aliquam dolor ut atque officiis. Ut adipisci quod ea recusandae sit delectus.', '25.00', NULL, 'DIGI277', 'instock', 0, 179, 'digital_17.jpg', NULL, 5, '2021-05-31 03:49:40', '2021-05-31 03:49:40'),
(22, 'cumque molestiae atque sit', 'cumque-molestiae-atque-sit', 'Eos omnis occaecati sunt totam et qui. Omnis iure totam hic. Iste a nihil odit occaecati veniam veritatis qui.', 'Maxime repellat libero architecto sunt beatae. Esse magni aperiam incidunt autem earum et. Molestias autem voluptas sunt ab. Qui molestias et consequuntur quaerat. Provident doloremque adipisci ut assumenda sit ut quas. Et nesciunt cupiditate excepturi. Dolorem veniam fugit similique in ratione aspernatur debitis. Error dolor distinctio aut ea rem. Soluta nobis odit expedita quia eius.', '422.00', NULL, 'DIGI178', 'instock', 0, 142, 'digital_20.jpg', NULL, 3, '2021-05-31 03:49:40', '2021-05-31 03:49:40');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('CfY8DJ1wnTxp0S8PrrP57ZOwGYxlFHd2br0JP3dr', 2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36 Edg/91.0.864.37', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiVHdaeFNraUNEME9zaDAxeWw0dnFzdTh1Y0VRNmxrWWY5djlhTTE5NyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjY6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9jYXJ0Ijt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MjtzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJDNBMFN1cU5pd3dWdW1PUHlkU3AxTXVlOFZvdUM0MllmNWNMQUFUWHhKb1ZXSEcuRUxpc1NhIjtzOjQ6ImNhcnQiO2E6MTp7czo3OiJkZWZhdWx0IjtPOjI5OiJJbGx1bWluYXRlXFN1cHBvcnRcQ29sbGVjdGlvbiI6MTp7czo4OiIAKgBpdGVtcyI7YToxOntzOjMyOiIwMjdjOTEzNDFmZDVjZjRkMjU3OWI0OWM0YjZhOTBkYSI7TzozMjoiR2xvdWRlbWFuc1xTaG9wcGluZ2NhcnRcQ2FydEl0ZW0iOjk6e3M6NToicm93SWQiO3M6MzI6IjAyN2M5MTM0MWZkNWNmNGQyNTc5YjQ5YzRiNmE5MGRhIjtzOjI6ImlkIjtpOjE7czozOiJxdHkiO2k6MjtzOjQ6Im5hbWUiO3M6MjI6ImF1dCBldCBjdXBpZGl0YXRlIHF1aXMiO3M6NToicHJpY2UiO2Q6Mjc4O3M6Nzoib3B0aW9ucyI7TzozOToiR2xvdWRlbWFuc1xTaG9wcGluZ2NhcnRcQ2FydEl0ZW1PcHRpb25zIjoxOntzOjg6IgAqAGl0ZW1zIjthOjA6e319czo0OToiAEdsb3VkZW1hbnNcU2hvcHBpbmdjYXJ0XENhcnRJdGVtAGFzc29jaWF0ZWRNb2RlbCI7czoxODoiQXBwXE1vZGVsc1xQcm9kdWN0IjtzOjQxOiIAR2xvdWRlbWFuc1xTaG9wcGluZ2NhcnRcQ2FydEl0ZW0AdGF4UmF0ZSI7aToyMTtzOjQxOiIAR2xvdWRlbWFuc1xTaG9wcGluZ2NhcnRcQ2FydEl0ZW0AaXNTYXZlZCI7YjowO319fX19', 1622444003);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `shippings`
--

CREATE TABLE `shippings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `line1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `line2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `mode` enum('card','paypal') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','approved','declined','refunded') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `utype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USR' COMMENT 'ADM for Admin and USR for User or Customer',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `utype`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@bircom.com', NULL, '$2y$10$dqno2Pjg5QpYdw9lP9vRIu/cG/vtXzXbVM7i/GJZz5DnmLeaLsHG2', NULL, NULL, NULL, NULL, NULL, 'ADM', '2021-05-31 03:50:13', '2021-05-31 03:50:13'),
(2, 'user', 'user@bircom.com', NULL, '$2y$10$3A0SuqNiwwVumOPydSp1Mue8VouC42Yf5cLAATXxJoVWHG.ELisSa', NULL, NULL, NULL, NULL, NULL, 'USR', '2021-05-31 03:51:48', '2021-05-31 03:51:48');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Tablo için indeksler `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Tablo için indeksler `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Tablo için indeksler `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_items_product_id_foreign` (`product_id`),
  ADD KEY `order_items_order_id_foreign` (`order_id`);

--
-- Tablo için indeksler `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Tablo için indeksler `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Tablo için indeksler `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Tablo için indeksler `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Tablo için indeksler `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shippings_order_id_foreign` (`order_id`);

--
-- Tablo için indeksler `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`),
  ADD KEY `transactions_order_id_foreign` (`order_id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Tablo için AUTO_INCREMENT değeri `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Tablo için AUTO_INCREMENT değeri `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Tablo için AUTO_INCREMENT değeri `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `shippings`
--
ALTER TABLE `shippings`
  ADD CONSTRAINT `shippings_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
